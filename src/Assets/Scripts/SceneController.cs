﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    
    public void Exit()
    {
        Application.Quit();
    }

    public void GoLevel1()
    {
        Data.Win = true;
        SceneManager.LoadScene("Level1");
    }

    public void GoStart()
    {
        SceneManager.LoadScene("Start");
    }

}
