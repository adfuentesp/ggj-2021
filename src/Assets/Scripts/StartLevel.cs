﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLevel : MonoBehaviour
{
    [SerializeField]
    private TilemapMask tilemapMask;

    [SerializeField]
    private float secondsToStart = 3f;

    [SerializeField]
    private PlayerController playerController;

    [SerializeField]
    private GameObject backgroundGood;
    [SerializeField]
    private GameObject backgroundEvil;

    void Start()
    {
        Invoke("ActivateGame", secondsToStart);
    }

    void ActivateGame()
    {
        tilemapMask.enabled = true;
        playerController.Ready = true;
        backgroundGood.SetActive(false);
        backgroundEvil.SetActive(true);
    }
}
