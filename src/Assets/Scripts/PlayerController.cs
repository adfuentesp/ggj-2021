﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController : MonoBehaviour
{
    [SerializeField]
    Animator animator;

    [SerializeField]
    public RecordAgent ra;

    [SerializeField]
    RoomManager rm;
    // Start is called before the first frame update
    [SerializeField]
    public Vector3Int cellPos;

    public Vector3 currentPos;
    public Vector3 nextPos;

    public int cellIdx;

    float timeDelta = 0;

    [SerializeField]
    float moveDurationFactor = 0.4f;
    float moveDuration;

    public bool ready = false;

    public bool Ready
    {
        get
        {
            return ready;
        }

        set
        {
            if (ready == value) return;
            ready = value;
            animator.SetBool("ready", value);
        }
    }

    public bool locked = true;


    public bool Locked
    {
        get
        {
            return locked;
        }

        set
        {
            if (locked == value) return;
            locked = value;
            animator.SetBool("walk", value);
        }
    }


    private AudioSource audioSource;
    [SerializeField]
    private AudioClip dieClip;
    [SerializeField]
    private AudioClip winClip;
    [SerializeField]
    private LevelManager levelManager;

    private void Start()
    {
        if (gameObject.CompareTag("Player"))
        {
            audioSource = GetComponent<AudioSource>();
        }
    }

    public void init()
    {
        if(rm == null)
        {
            rm = GameObject.Find("RoomTilemap").GetComponent<RoomManager>();
        }
        cellPos = rm.WorldToCellPos(transform.position);
        transform.position = rm.CellPosToWorld(cellPos);
        refreshCellIdx();
        nextPos = transform.position;
    }


    void Update()
    {
        if (!ready) return;   
        if (transform.position == nextPos)
        {

            timeDelta = 0;
            Locked = false;
            return;
        }
        Locked = true;

        timeDelta += Time.deltaTime;
        Vector3 v3 = Vector3.Lerp(currentPos, nextPos, timeDelta / moveDuration);
        transform.position = v3;
    }

    public void move(Vector3Int cellMove)
    {
        cellPos += cellMove;
        refreshCellIdx();
        moveDuration = Vector3.Magnitude(cellMove) * moveDurationFactor;
        currentPos = transform.position;
        nextPos = rm.CellPosToWorld(cellPos);
        Vector3 dir = (nextPos - currentPos).normalized;

        animator.SetFloat("dirY", dir.y);
        animator.SetFloat("dirX", dir.x);
        if (ra != null)
        {

            ra.Add(new RecordInput(dir, currentPos, nextPos, cellMove));
        }
    }

    public void refreshCellIdx()
    {
        cellIdx = rm.GetCellIndex(cellPos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!gameObject.CompareTag("Player")) return;

        if (collision.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<PlayerController>().ready = false;
            ready = false;
            StartCoroutine(CrDie());
        }
        if (collision.CompareTag("Bed"))
        {
            ready = false;
            StartCoroutine(CrFinishLevel());
        }
    }

    private IEnumerator CrDie()
    {
        audioSource.PlayOneShot(dieClip);
        yield return new WaitForSeconds(2.5f);
        Data.Win = false;
        levelManager.EndGame();
        yield return null;
    }

    private IEnumerator CrFinishLevel()
    {
        audioSource.PlayOneShot(winClip);
        yield return new WaitForSeconds(2.5f);
        levelManager.GoNextLevel();
        yield return null;
    }

    
}
