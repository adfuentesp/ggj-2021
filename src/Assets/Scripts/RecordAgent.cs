﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct RecordInput
{
    public Vector3 dir;
    public Vector3 fromPos;
    public Vector3 toPos;
    public Vector3Int cellMove;

    public RecordInput(Vector3 dir, Vector3 from, Vector3 to, Vector3Int cellMove)
    {
        this.dir = dir;
        this.fromPos = from;
        this.toPos = to;
        this.cellMove = cellMove;
    }
}

public class RecordAgent : MonoBehaviour
{

    ArrayList inputs = new ArrayList();
    [SerializeField]
    int nInputs = 0;

    [SerializeField]
    int spawnEnemyAtNsteps;

    [SerializeField]
    GameObject enemy;
    public PlayerController enemyRef;

    [SerializeField]
    Transform grid;

    public void Add(RecordInput ri)
    {
        inputs.Add(ri);

        nInputs++;
        if(nInputs == spawnEnemyAtNsteps)
        {
            RecordInput firstRecord = (RecordInput)inputs[0];
            GameObject go = Instantiate(enemy, firstRecord.fromPos,Quaternion.identity,grid);
            enemyRef = go.GetComponent<PlayerController>();
            GameObject.Find("Lighting Enemy").GetComponent<Lighting>().target = go;
            enemyRef.init();
        }
        if (nInputs >= spawnEnemyAtNsteps)
        {
            RecordInput currentEnemyRecord = (RecordInput)inputs[nInputs-spawnEnemyAtNsteps];
            enemyRef.Locked = false;
            enemyRef.Ready = true;
            enemyRef.move(currentEnemyRecord.cellMove);
        }
    }
}
