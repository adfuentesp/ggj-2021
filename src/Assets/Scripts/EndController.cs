﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndController : MonoBehaviour
{
    float timeDelta = 0;
    public float transition = 0.5f;
    public UnityEngine.UI.Image background;
    public Sprite[] imagesWin;
    public Sprite[] imagesLost;

    int idx = 0;
    int setLength = 1;
    public Sprite[] selectedSet;

    [SerializeField]
    private AudioClip[] audioClip = new AudioClip[2];
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (Data.Win)
        {
            selectedSet = imagesWin;
            audioSource.clip = audioClip[0];
            audioSource.loop = true;
            audioSource.Play();
        } else
        {
            selectedSet = imagesLost;
            audioSource.clip = audioClip[1];
            audioSource.loop = true;
            audioSource.Play();
        }
        setLength = selectedSet.Length;
        timeDelta = transition;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(idx % setLength);
        timeDelta += Time.deltaTime;

        if(timeDelta >= transition)
        {
            timeDelta = 0;
            background.sprite = selectedSet[idx % setLength];
            idx++;
        }
    }
}
