﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public class  RoomManager : MonoBehaviour
{
    BoundsInt bounds;
    Tilemap tilemap;
    ArrayList[] allTilesKeyArray;

    GridLayout gridLayout;
    [SerializeField]
    PlayerController player;
    void Start()
    {
        gridLayout = transform.parent.GetComponentInParent<GridLayout>();
        tilemap = GetComponent<Tilemap>();

        bounds = tilemap.cellBounds;
        TileBase[] allTiles = tilemap.GetTilesBlock(bounds);

        allTilesKeyArray = new ArrayList[bounds.size.x * bounds.size.y];
        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                int idx = x + y * bounds.size.x;
                TileBase tile = allTiles[x + y * bounds.size.x];
                
                if (tile != null)
                {
                    string name = tile.name;

                    allTilesKeyArray[idx] = new ArrayList();
                    int n_dirs = name.Length;
                    char[] dirs = name.ToCharArray();
                    foreach(char dir in dirs)
                    {
                        allTilesKeyArray[idx].Add(CharToKey(dir));
                    }
                }
            }
        }

        player.init();
    }


    public Vector3Int WorldToCellPos(Vector3 pos)
    {
        return gridLayout.WorldToCell(pos);
    }

    public Vector3 CellPosToWorld(Vector3Int cellPos)
    {
        return tilemap.GetCellCenterWorld(cellPos);
    }

    public int GetCellIndex(Vector3Int cellPos)
    {
        Vector3Int botleft = bounds.position;
        Vector3Int cellDiff = cellPos - botleft;
        int idx = cellDiff.x + cellDiff.y * bounds.size.x;
        TileBase[] allTiles = tilemap.GetTilesBlock(bounds);


        return idx;
    }

    public void movePlayerCell(KeyCode k, Vector3Int movement)
    {
        Vector3Int acc_movement = movement;
        switch (k)
        {
            case KeyCode.UpArrow:{ acc_movement += new Vector3Int(0, 1, 0); }break;
            case KeyCode.DownArrow: { acc_movement += new Vector3Int(0, -1, 0); } break;
            case KeyCode.LeftArrow: { acc_movement += new Vector3Int(-1, 0, 0); } break;
            case KeyCode.RightArrow: { acc_movement += new Vector3Int(1, 0, 0); } break;
        }
        int next_index = GetCellIndex(player.cellPos + acc_movement);
        ArrayList next_directions = allTilesKeyArray[next_index];
        if (next_directions.Contains(k))
        {
            movePlayerCell(k,acc_movement);
        } else
        {
            player.move(acc_movement);
        }
    }

    void Update()
    {
        foreach (KeyCode k in allTilesKeyArray[player.cellIdx])
        {
            if (Input.GetKeyDown(k) && !isRoomLocked())
            {
                movePlayerCell(k, Vector3Int.zero);
            }
        }
    }

    static KeyCode CharToKey(char c)
    {
        switch (c)
        {
            case 'r': { return KeyCode.RightArrow; };
            case 'l': { return KeyCode.LeftArrow; };
            case 't': { return KeyCode.UpArrow; };
            case 'd': { return KeyCode.DownArrow; };
            default: return KeyCode.UpArrow;
        }
    } 


    bool isRoomLocked()
    {
        bool firstCondition = player.ra.enemyRef == null ? false : player.ra.enemyRef.locked;
        return firstCondition || player.locked;
    }
}
