﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lighting : MonoBehaviour
{
    [Range(0.02f, 0.09f)]
    public float addSize;
    [Range(0.01f, 1f)]
    public float animationDuration;
    public int animationSteps = 20;

    // Ability
    [SerializeField]
    private Text cdText;
    [Range(1f, 5f)]
    public float abilityMaxSize;
    [Range(0.01f, 1f)]
    public float abilityDuration;
    public int abilitySteps = 60;
    public float abilityKept = 1.5f;
    public float abilityCooldownTime = 10f;

    private float cooldown = 0.0f;
    private float timer = 0;
    private bool tickling = false;
    private bool activate = false;

    private Vector3 originalScale;

    public GameObject target;

    [SerializeField]
    private Image abilityIcon;
    [SerializeField]
    private Sprite[] abilityStates = new Sprite[3];

    void Start()
    {
        originalScale = transform.localScale;
    }

    void Update()
    {
        // Normal tickling
        if (!tickling && !activate) {
            tickling = true;
            StartCoroutine(CrTickling());
        }
        // Use ability
        if ((target != null) && target.CompareTag("Player") && Input.GetKeyDown(KeyCode.Space) && (cooldown <= 0.0f))
        {
            cooldown = abilityCooldownTime;
            activate = true;
            if (tickling)
            {
                StopCoroutine(CrTickling());
                StartCoroutine(CrAbility());
            }            
        }
        // Ability cd
        if (cooldown >= 0)
        {
            cooldown -= Time.deltaTime;
            if (cooldown < 0)
            {
                cooldown = 0;
            }
        }
        // RefreshUI
        RefreshUI();
        // Follow
        FollowTarget();
    }

    IEnumerator CrTickling()
    {
        float stepTime = animationDuration / animationSteps;
        int steps = animationSteps / 2;
        // Maximize
        for (int i = 0; i < steps; i++) {
            ChangeScale(addSize);
            yield return new WaitForSeconds(stepTime);
        }
        // Minimize
        for (int i = 0; i < steps; i++)
        {
            ChangeScale(-addSize);
            yield return new WaitForSeconds(stepTime);
        }
        tickling = false;

        yield return null;
    }

    IEnumerator CrAbility()
    {
        float stepTime = abilityDuration / abilitySteps;
        int steps = abilitySteps / 2;
        // Maximize
        float amount = (abilityMaxSize - transform.localScale.x) / steps;
        for (int i = 0; i < steps; i++)
        {
            ChangeScale(amount);
            yield return new WaitForSeconds(stepTime);
        }
        // Kept
        StartCoroutine(CrTickling());
        yield return new WaitForSeconds(abilityKept);
        StopCoroutine(CrTickling());
        // Minimize
        amount = (originalScale.x - transform.localScale.x) / steps;
        for (int i = 0; i < steps; i++)
        {
            ChangeScale(amount, 1.0f);
            yield return new WaitForSeconds(stepTime);
        }
        activate = false;

        yield return null;
    }
    
    private void ChangeScale(float amount, float f = 0.5f)
    {
        Vector3 newScale = new Vector3(transform.localScale.x + amount, transform.localScale.y + amount, transform.localScale.z);
        transform.localScale = Vector3.Lerp(transform.localScale, newScale, f);
    }

    private void RefreshUI()
    {
        if ((target == null) || !target.CompareTag("Player")) return; 

        string cdShown = cooldown.ToString("F1");
        if (cdShown.Equals("0,0")) {
            cdShown = "";
        }
        cdText.text = cdShown;
        if (activate)
        {
            abilityIcon.sprite = abilityStates[2];
        } else
        {
            if (cooldown == 0.0f)
            {
                abilityIcon.sprite = abilityStates[1];
            } else
            {
                abilityIcon.sprite = abilityStates[0];
            }
        }
    }

    private void FollowTarget()
    {
        if (target != null)
        {
            transform.position = target.transform.position;
        }
    }
}
