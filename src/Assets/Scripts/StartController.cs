﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartController : MonoBehaviour
{
    [SerializeField]
    private GameObject controlsPanel;
    [SerializeField]
    private GameObject authorsPanel;

    public void ToggleControls()
    {
        controlsPanel.SetActive(!controlsPanel.activeSelf);
    }

    public void ToggleAuthors()
    {
        authorsPanel.SetActive(!authorsPanel.activeSelf);
    }
}
